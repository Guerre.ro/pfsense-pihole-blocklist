# Blocklist for Pi-Hole
Handpicked custom blocklist/blacklist annoying ads, trackers, scam sites, email spam websites, malware, honeypot traps and govs!

![](https://raw.githubusercontent.com/einyx/pihole-blocklist/master/img/logo.jpg)

|Listname|URL|
|--|--|
|blacklist|https://raw.githubusercontent.com/einyx/einyx-blocklist/master/|

**INFO:**

**Feel free to share it with others!** I will try to keep it up-to-date regularly.

---

Easy to copy URLs:
https://gitlab.com/Guerre.ro/pfsense-pihole-blocklist/-/raw/master/ads.txt
https://gitlab.com/Guerre.ro/pfsense-pihole-blocklist/-/raw/master/compromised.txt
https://gitlab.com/Guerre.ro/pfsense-pihole-blocklist/-/raw/master/facebook.txt
https://gitlab.com/Guerre.ro/pfsense-pihole-blocklist/-/raw/master/gaming.txt
https://gitlab.com/Guerre.ro/pfsense-pihole-blocklist/-/raw/master/malware.txt
https://gitlab.com/Guerre.ro/pfsense-pihole-blocklist/-/raw/master/mining.txt
https://gitlab.com/Guerre.ro/pfsense-pihole-blocklist/-/raw/master/nsa.txt
https://gitlab.com/Guerre.ro/pfsense-pihole-blocklist/-/raw/master/philips.txt
https://gitlab.com/Guerre.ro/pfsense-pihole-blocklist/-/raw/master/phishing.txt
https://gitlab.com/Guerre.ro/pfsense-pihole-blocklist/-/raw/master/sonos.txt
https://gitlab.com/Guerre.ro/pfsense-pihole-blocklist/-/raw/master/spotify.txt
https://gitlab.com/Guerre.ro/pfsense-pihole-blocklist/-/raw/master/windows.txt
https://gitlab.com/Guerre.ro/pfsense-pihole-blocklist/-/raw/master/xiaomi.txt
